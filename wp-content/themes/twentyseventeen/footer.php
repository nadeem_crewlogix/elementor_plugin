<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="wrap">
				<?php
				get_template_part( 'template-parts/footer/footer', 'widgets' );

				if ( has_nav_menu( 'social' ) ) : ?>
					<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentyseventeen' ); ?>">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'social',
								'menu_class'     => 'social-links-menu',
								'depth'          => 1,
								'link_before'    => '<span class="screen-reader-text">',
								'link_after'     => '</span>' . twentyseventeen_get_svg( array( 'icon' => 'chain' ) ),
							) );
						?>
					</nav><!-- .social-navigation -->
				<?php endif;

				get_template_part( 'template-parts/footer/site', 'info' );
				?>
			</div><!-- .wrap -->

		</footer><!-- #colophon -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
      	 <form method="POST" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Title:</h4>
        <p><input type="text" name="title"></p>       
         </div>
        <div class="modal-body">
          <p>Description:</p>
          <p><input type="text" name="description"></p>
        </div>
        <div class="modal-footer">
        	<button type="submit" id="submit-form" class="btn btn-primary" name="submit" data-dismiss="modal">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </form>
      </div>
      
    </div>
  </div>
	</div><!-- .site-content-contain -->
</div><!-- #page -->


<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/bootstrap.min.js'; ?>"> </script>
<script type="text/javascript">



    
    jQuery(document).ready(function($) {
        
		$(document).on('click','#crew-add-page-btn',function(){
			alert('ADD PAGE ');
			$('#myModal').modal('show');
		});

	 	$('#submit-form').click(function(e){	 	
	 		var target = $(this);
	 		var myData =  {
		        	action: 'my_ajax_action',
		            title : $('input[name="title"]').val(), // PHP: $_POST['first_name']
		            description  : $('input[name="description"]').val(), // PHP: $_POST['last_name']
		        }
	 		var callBackUrl = my_ajaxurl;
	 		e.preventDefault();    
		    $.ajax({
		        type: "POST", // use $_POST method to submit data
		        url: callBackUrl, // where to submit the data
		       data : myData,
		        success:function(data) {
		            console.log(data); // the HTML result of that URL after submit the data
		        },
		        error: function(errorThrown){
		            console.log(errorThrown); // error
		        }
		    });  
	 	});
  
});

</script>

<?php wp_footer(); ?>

</body>
</html>
