<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Settings extends Settings_Page {

	const PAGE_ID = 'simpleweb';

	const MENU_PRIORITY_GO_PRO = 502;

	const UPDATE_TIME_FIELD = '_simpleweb_settings_update_time';

	const TAB_GENERAL = 'general';
	const TAB_STYLE = 'style';
	const TAB_INTEGRATIONS = 'integrations';
	const TAB_ADVANCED = 'advanced';

	public function register_admin_menu() {
		add_menu_page(
			__( 'SimpleWeb', 'simpleweb' ),
			__( 'SimpleWeb', 'simpleweb' ),
			'manage_options',
			self::PAGE_ID,
			[ $this, 'display_settings_page' ],
			'',
			99
		);
	}

	public function register_pro_menu() {
		add_submenu_page(
			self::PAGE_ID,
			'',
			'<span class="dashicons dashicons-star-filled" style="font-size: 17px"></span> ' . __( 'Go Pro', 'simpleweb' ),
			'manage_options',
			'go_simpleweb_pro',
			[ $this, 'go_simpleweb_pro' ]
		);
	}

	public function go_simpleweb_pro() {
		if ( isset( $_GET['page'] ) && 'go_simpleweb_pro' === $_GET['page'] ) {
			wp_redirect( 'https://go.simpleweb.com/pro-admin-menu/' );
		}
	}

	public function admin_menu_change_name() {
		global $submenu;

		if ( isset( $submenu['simpleweb'] ) )
			$submenu['simpleweb'][0][0] = __( 'Settings', 'simpleweb' );
	}

	public function __construct() {
		parent::__construct();

		include( ELEMENTOR_PATH . 'includes/settings/controls.php' );
		include( ELEMENTOR_PATH . 'includes/settings/validations.php' );

		add_action( 'admin_init', [ $this, 'go_simpleweb_pro' ] );
		add_action( 'admin_menu', [ $this, 'register_admin_menu' ], 20 );
		add_action( 'admin_menu', [ $this, 'admin_menu_change_name' ], 200 );
		add_action( 'admin_menu', [ $this, 'register_pro_menu' ], self::MENU_PRIORITY_GO_PRO );
	}

	protected function create_tabs() {
		$validations_class_name = __NAMESPACE__ . '\Settings_Validations';

		return [
			self::TAB_GENERAL => [
				'label' => __( 'General', 'simpleweb' ),
				'sections' => [
					'general' => [
						'fields' => [
							self::UPDATE_TIME_FIELD => [
								'full_field_id' => self::UPDATE_TIME_FIELD,
								'field_args' => [
									'type' => 'hidden',
								],
								'setting_args' => [
									'sanitize_callback' => 'time',
								],
							],
							'cpt_support' => [
								'label' => __( 'Post Types', 'simpleweb' ),
								'field_args' => [
									'type' => 'checkbox_list_cpt',
									'std' => [ 'page', 'post' ],
									'exclude' => [ 'attachment', 'simpleweb_library' ],
								],
								'setting_args' => [ $validations_class_name, 'checkbox_list' ],
							],
							'exclude_user_roles' => [
								'label' => __( 'Exclude Roles', 'simpleweb' ),
								'field_args' => [
									'type' => 'checkbox_list_roles',
									'exclude' => [ 'administrator' ],
								],
								'setting_args' => [ $validations_class_name, 'checkbox_list' ],
							],
							'disable_color_schemes' => [
								'label' => __( 'Disable Global Colors', 'simpleweb' ),
								'field_args' => [
									'type' => 'checkbox',
									'value' => 'yes',
									'sub_desc' => __( 'Checking this box will disable SimpleWeb\'s Global Colors, and make SimpleWeb inherit the colors from your theme.', 'simpleweb' ),
								],
							],
							'disable_typography_schemes' => [
								'label' => __( 'Disable Global Fonts', 'simpleweb' ),
								'field_args' => [
									'type' => 'checkbox',
									'value' => 'yes',
									'sub_desc' => __( 'Checking this box will disable SimpleWeb\'s Global Fonts, and make SimpleWeb inherit the fonts from your theme.', 'simpleweb' ),
								],
							],
						],
					],
					'usage' => [
						'label' => __( 'Improve SimpleWeb', 'simpleweb' ),
						'fields' => [
							'allow_tracking' => [
								'label' => __( 'Usage Data Tracking', 'simpleweb' ),
								'field_args' => [
									'type' => 'checkbox',
									'value' => 'yes',
									'default' => '',
									'sub_desc' => __( 'Opt-in to our anonymous plugin data collection and to updates. We guarantee no sensitive data is collected.', 'simpleweb' ) . sprintf( ' <a href="%s" target="_blank">%s</a>', 'https://go.simpleweb.com/usage-data-tracking/', __( 'Learn more.', 'simpleweb' ) ),
								],
								'setting_args' => [ __NAMESPACE__ . '\Tracker', 'check_for_settings_optin' ],
							],
						],
					],
				],
			],
			self::TAB_STYLE => [
				'label' => __( 'Style', 'simpleweb' ),
				'sections' => [
					'style' => [
						'fields' => [
							'default_generic_fonts' => [
								'label' => __( 'Default Generic Fonts', 'simpleweb' ),
								'field_args' => [
									'type' => 'text',
									'std' => 'Sans-serif',
									'class' => 'medium-text',
									'desc' => __( 'The list of fonts used if the chosen font is not available.', 'simpleweb' ),
								],
							],
							'container_width' => [
								'label' => __( 'Content Width', 'simpleweb' ),
								'field_args' => [
									'type' => 'text',
									'placeholder' => '1140',
									'sub_desc' => 'px',
									'class' => 'medium-text',
									'desc' => __( 'Sets the default width of the content area (Default: 1140)', 'simpleweb' ),
								],
							],
							'space_between_widgets' => [
								'label' => __( 'Space Between Widgets', 'simpleweb' ),
								'field_args' => [
									'type' => 'text',
									'placeholder' => '20',
									'sub_desc' => 'px',
									'class' => 'medium-text',
									'desc' => __( 'Sets the default space between widgets (Default: 20)', 'simpleweb' ),
								],
							],
							'stretched_section_container' => [
								'label' => __( 'Stretched Section Fit To', 'simpleweb' ),
								'field_args' => [
									'type' => 'text',
									'placeholder' => 'body',
									'class' => 'medium-text',
									'desc' => __( 'Enter parent element selector to which stretched sections will fit to (e.g. #primary / .wrapper / main etc). Leave blank to fit to page width.', 'simpleweb' ),
								],
							],
							'page_title_selector' => [
								'label' => __( 'Page Title Selector', 'simpleweb' ),
								'field_args' => [
									'type' => 'text',
									'placeholder' => 'h1.entry-title',
									'class' => 'medium-text',
									'desc' => __( 'SimpleWeb lets you hide the page title. This works for themes that have "h1.entry-title" selector. If your theme\'s selector is different, please enter it above.', 'simpleweb' ),
								],
							],
						],
					],
				],
			],
			self::TAB_INTEGRATIONS => [
				'label' => __( 'Integrations', 'simpleweb' ),
				'sections' => [],
			],
			self::TAB_ADVANCED => [
				'label' => __( 'Advanced', 'simpleweb' ),
				'sections' => [
					'advanced' => [
						'fields' => [
							'css_print_method' => [
								'label' => __( 'CSS Print Method', 'simpleweb' ),
								'field_args' => [
									'class' => 'simpleweb_css_print_method',
									'type' => 'select',
									'options' => [
										'external' => __( 'External File', 'simpleweb' ),
										'internal' => __( 'Internal Embedding', 'simpleweb' ),
									],
									'desc' => '<div class="simpleweb-css-print-method-description" data-value="external" style="display: none">' .
									          __( 'Use external CSS files for all generated stylesheets. Choose this setting for better performance (recommended).', 'simpleweb' ) .
									          '</div>' .
									          '<div class="simpleweb-css-print-method-description" data-value="internal" style="display: none">' .
									          __( 'Use internal CSS that is embedded in the head of the page. For troubleshooting server configuration conflicts and managing development environments.', 'simpleweb' ) .
									          '</div>',
								],
							],
							'editor_break_lines' => [
								'label' => __( 'Switch Editor Loader Method', 'simpleweb' ),
								'field_args' => [
									'type' => 'select',
									'options' => [
										'' => __( 'Disable', 'simpleweb' ),
										1 => __( 'Enable', 'simpleweb' ),
									],
									'desc' => __( 'For troubleshooting server configuration conflicts.', 'simpleweb' ),
								],
							],
						],
					],
				],
			],
		];
	}

	protected function get_page_title() {
		return __( 'SimpleWeb', 'simpleweb' );
	}
}
