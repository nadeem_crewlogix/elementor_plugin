<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Tools extends Settings_Page {

	const PAGE_ID = 'simpleweb-tools';

	public function register_admin_menu() {
		add_submenu_page(
			Settings::PAGE_ID,
			__( 'Tools', 'simpleweb' ),
			__( 'Tools', 'simpleweb' ),
			'manage_options',
			self::PAGE_ID,
			[ $this, 'display_settings_page' ]
		);
	}

	public function ajax_simpleweb_clear_cache() {
		check_ajax_referer( 'simpleweb_clear_cache', '_nonce' );

		Plugin::$instance->posts_css_manager->clear_cache();

		wp_send_json_success();
	}

	public function ajax_simpleweb_replace_url() {
		check_ajax_referer( 'simpleweb_replace_url', '_nonce' );

		$from = ! empty( $_POST['from'] ) ? trim( $_POST['from'] ) : '';
		$to = ! empty( $_POST['to'] ) ? trim( $_POST['to'] ) : '';

		$is_valid_urls = ( filter_var( $from, FILTER_VALIDATE_URL ) && filter_var( $to, FILTER_VALIDATE_URL ) );
		if ( ! $is_valid_urls ) {
			wp_send_json_error( __( 'The `from` and `to` URL\'s must be a valid URL', 'simpleweb' ) );
		}

		if ( $from === $to ) {
			wp_send_json_error( __( 'The `from` and `to` URL\'s must be different', 'simpleweb' ) );
		}

		global $wpdb;

		// @codingStandardsIgnoreStart cannot use `$wpdb->prepare` because it remove's the backslashes
		$rows_affected = $wpdb->query(
			"UPDATE {$wpdb->postmeta} " .
			"SET `meta_value` = REPLACE(`meta_value`, '" . str_replace( '/', '\\\/', $from ) . "', '" . str_replace( '/', '\\\/', $to ) . "') " .
			"WHERE `meta_key` = '_simpleweb_data' AND `meta_value` LIKE '[%' ;" ); // meta_value LIKE '[%' are json formatted
		// @codingStandardsIgnoreEnd

		if ( false === $rows_affected ) {
			wp_send_json_error( __( 'An error occurred', 'simpleweb' ) );
		} else {
			Plugin::$instance->posts_css_manager->clear_cache();
			wp_send_json_success( sprintf( __( '%d Rows Affected', 'simpleweb' ), $rows_affected ) );
		}
	}

	public function post_simpleweb_rollback() {
		check_admin_referer( 'simpleweb_rollback' );

		$plugin_slug = basename( ELEMENTOR__FILE__, '.php' );

		$rollback = new Rollback( [
			'version' => ELEMENTOR_PREVIOUS_STABLE_VERSION,
			'plugin_name' => ELEMENTOR_PLUGIN_BASE,
			'plugin_slug' => $plugin_slug,
			'package_url' => sprintf( 'https://downloads.wordpress.org/plugin/%s.%s.zip', $plugin_slug, ELEMENTOR_PREVIOUS_STABLE_VERSION ),
		] );

		$rollback->run();

		wp_die( '', __( 'Rollback to Previous Version', 'simpleweb' ), [ 'response' => 200 ] );
	}

	public function __construct() {
		parent::__construct();

		add_action( 'admin_menu', [ $this, 'register_admin_menu' ], 205 );

		if ( ! empty( $_POST ) ) {
			add_action( 'wp_ajax_simpleweb_clear_cache', [ $this, 'ajax_simpleweb_clear_cache' ] );
			add_action( 'wp_ajax_simpleweb_replace_url', [ $this, 'ajax_simpleweb_replace_url' ] );
		}

		add_action( 'admin_post_simpleweb_rollback', [ $this, 'post_simpleweb_rollback' ] );
	}

	protected function create_tabs() {
		return [
			'general' => [
				'label' => __( 'General', 'simpleweb' ),
				'sections' => [
					'tools' => [
						'fields' => [
							'clear_cache' => [
								'label' => __( 'Regenerate CSS', 'simpleweb' ),
								'field_args' => [
									'type' => 'raw_html',
									'html' => sprintf( '<button data-nonce="%s" class="button simpleweb-button-spinner" id="simpleweb-clear-cache-button">%s</button>', wp_create_nonce( 'simpleweb_clear_cache' ), __( 'Regenerate Files', 'simpleweb' ) ),
									'desc' => __( 'Styles set in SimpleWeb are saved in CSS files in the uploads folder. Recreate those files, according to the most recent settings.', 'simpleweb' ),
								],
							],
							'reset_api_data' => [
								'label' => __( 'Sync Library', 'simpleweb' ),
								'field_args' => [
									'type' => 'raw_html',
									'html' => sprintf( '<button data-nonce="%s" class="button simpleweb-button-spinner" id="simpleweb-library-sync-button">%s</button>', wp_create_nonce( 'simpleweb_reset_library' ), __( 'Sync Library', 'simpleweb' ) ),
									'desc' => __( 'SimpleWeb Library automatically updates on a daily basis. You can also manually update it by clicking on the sync button.', 'simpleweb' ),
								],
							],
						],
					],
				],
			],
			'replace_url' => [
				'label' => __( 'Replace URL', 'simpleweb' ),
				'sections' => [
					'replace_url' => [
						'callback' => function() {
							$intro_text = sprintf( __( '<strong>Important:</strong> It is strongly recommended that you <a target="_blank" href="%s">backup your database</a> before using Replace URL.', 'simpleweb' ), 'https://codex.wordpress.org/WordPress_Backups' );
							$intro_text = '<div>' . $intro_text . '</div>';

							echo $intro_text;
						},
						'fields' => [
							'replace_url' => [
								'label' => __( 'Update Site Address (URL)', 'simpleweb' ),
								'field_args' => [
									'type' => 'raw_html',
									'html' => sprintf( '<input type="text" name="from" placeholder="http://old-url.com" class="medium-text"><input type="text" name="to" placeholder="http://new-url.com" class="medium-text"><button data-nonce="%s" class="button simpleweb-button-spinner" id="simpleweb-replace-url-button">%s</button>', wp_create_nonce( 'simpleweb_replace_url' ), __( 'Replace URL', 'simpleweb' ) ),
									'desc' => __( 'Enter your old and new URLs for your WordPress installation, to update all SimpleWeb data (Relevant for domain transfers or move to \'HTTPS\').', 'simpleweb' ),
								],
							],
						],
					],
				],
			],
			'versions' => [
				'label' => __( 'Version Control', 'simpleweb' ),
				'sections' => [
					'rollback' => [
						'label' => __( 'Rollback to Previous Version', 'simpleweb' ),
						'callback' => function() {
							$intro_text = sprintf( __( 'Experiencing an issue with SimpleWeb version %s? Rollback to a previous version before the issue appeared.', 'simpleweb' ), ELEMENTOR_VERSION );
							$intro_text = '<p>' . $intro_text . '</p>';

							echo $intro_text;
						},
						'fields' => [
							'rollback' => [
								'label' => __( 'Rollback Version', 'simpleweb' ),
								'field_args' => [
									'type' => 'raw_html',
									'html' => sprintf( '<a href="%s" class="button simpleweb-button-spinner simpleweb-rollback-button">%s</a>', wp_nonce_url( admin_url( 'admin-post.php?action=simpleweb_rollback' ), 'simpleweb_rollback' ), sprintf( __( 'Reinstall v%s', 'simpleweb' ), ELEMENTOR_PREVIOUS_STABLE_VERSION ) ),
									'desc' => '<span style="color: red;">' . __( 'Warning: Please backup your database before making the rollback.', 'simpleweb' ) . '</span>',
								],
							],
						],
					],
					'beta' => [
						'label' => __( 'Become a Beta Tester', 'simpleweb' ),
						'callback' => function() {
							$intro_text = sprintf( __( 'Turn-on Beta Tester, to get notified when a new beta version of SimpleWeb or E-Pro is available. The Beta version will not install automatically. You always have the option to ignore it.', 'simpleweb' ), ELEMENTOR_VERSION );
							$intro_text = '<p>' . $intro_text . '</p>';

							echo $intro_text;
						},
						'fields' => [
							'beta' => [
								'label' => __( 'Beta Tester', 'simpleweb' ),
								'field_args' => [
									'type' => 'select',
									'default' => 'no',
									'options' => [
										'no' => __( 'Disable', 'simpleweb' ),
										'yes' => __( 'Enable', 'simpleweb' ),
									],
									'desc' => __( 'Please Note: We do not recommend updating to a beta version on production sites.', 'simpleweb' ),
								],
							],
						],
					],
				],
			],
		];
	}

	public function display_settings_page() {
		wp_enqueue_script( 'simpleweb-dialog' );

		parent::display_settings_page();
	}

	protected function get_page_title() {
		return __( 'Tools', 'simpleweb' );
	}
}
