<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<script type="text/template" id="tmpl-simpleweb-panel">
	<div id="simpleweb-mode-switcher"></div>
	<header id="simpleweb-panel-header-wrapper"></header>
	<main id="simpleweb-panel-content-wrapper"></main>
	<footer id="simpleweb-panel-footer">
		<div class="simpleweb-panel-container">
		</div>
	</footer>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-menu-item">
	<div class="simpleweb-panel-menu-item-icon">
		<i class="{{ icon }}"></i>
	</div>
	<div class="simpleweb-panel-menu-item-title">{{{ title }}}</div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-header">
	<div id="simpleweb-panel-header-menu-button" class="simpleweb-header-button">
		<i class="simpleweb-icon eicon-menu tooltip-target" data-tooltip="<?php esc_attr_e( 'Menu', 'simpleweb' ); ?>"></i>
	</div>
<!-- 	<div id="simpleweb-panel-header-title"></div> -->
<div class="text-center panel-simpleheading"><h4 style="font-size: 18px;
	font-style: italic;">Simpleweb</h4></div>
	<div id="simpleweb-panel-header-add-button" class="simpleweb-header-button">
		<i class="simpleweb-icon eicon-apps tooltip-target" data-tooltip="<?php esc_attr_e( 'Widgets Panel', 'simpleweb' ); ?>"></i>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-footer-content">
	<div id="simpleweb-panel-footer-exit" class="simpleweb-panel-footer-tool" title="<?php _e( 'Exit', 'simpleweb' ); ?>">
		<i class="fa fa-times"></i>
		<div class="simpleweb-panel-footer-sub-menu-wrapper">
			<div class="simpleweb-panel-footer-sub-menu">
				<a id="simpleweb-panel-footer-view-page" class="simpleweb-panel-footer-sub-menu-item" href="<?php the_permalink(); ?>" target="_blank">
					<i class="simpleweb-icon fa fa-external-link"></i>
					<span class="simpleweb-title"><?php _e( 'View Page', 'simpleweb' ); ?></span>
				</a>
				<a id="simpleweb-panel-footer-view-edit-page" class="simpleweb-panel-footer-sub-menu-item" href="<?php echo get_edit_post_link(); ?>">
					<i class="simpleweb-icon fa fa-wordpress"></i>
					<span class="simpleweb-title"><?php _e( 'Go to Dashboard', 'simpleweb' ); ?></span>
				</a>
			</div>
		</div>
	</div>
	<div id="simpleweb-panel-footer-responsive" class="simpleweb-panel-footer-tool" title="<?php esc_attr_e( 'Responsive Mode', 'simpleweb' ); ?>">
		<i class="eicon-device-desktop"></i>
		<div class="simpleweb-panel-footer-sub-menu-wrapper">
			<div class="simpleweb-panel-footer-sub-menu">
				<div class="simpleweb-panel-footer-sub-menu-item" data-device-mode="desktop">
					<i class="simpleweb-icon eicon-device-desktop"></i>
					<span class="simpleweb-title"><?php _e( 'Desktop', 'simpleweb' ); ?></span>
					<span class="simpleweb-description"><?php _e( 'Default Preview', 'simpleweb' ); ?></span>
				</div>
				<div class="simpleweb-panel-footer-sub-menu-item" data-device-mode="tablet">
					<i class="simpleweb-icon eicon-device-tablet"></i>
					<span class="simpleweb-title"><?php _e( 'Tablet', 'simpleweb' ); ?></span>
					<span class="simpleweb-description"><?php _e( 'Preview for 768px', 'simpleweb' ); ?></span>
				</div>
				<div class="simpleweb-panel-footer-sub-menu-item" data-device-mode="mobile">
					<i class="simpleweb-icon eicon-device-mobile"></i>
					<span class="simpleweb-title"><?php _e( 'Mobile', 'simpleweb' ); ?></span>
					<span class="simpleweb-description"><?php _e( 'Preview for 360px', 'simpleweb' ); ?></span>
				</div>
			</div>
		</div>
	</div>
	<div id="simpleweb-panel-footer-help" style="display:none;" class="simpleweb-panel-footer-tool" title="<?php esc_attr_e( 'Help', 'simpleweb' ); ?>">
		<span class="simpleweb-screen-only"><?php _e( 'Help', 'simpleweb' ); ?></span>
		<i class="fa fa-question-circle"></i>
		<div class="simpleweb-panel-footer-sub-menu-wrapper">
			<div class="simpleweb-panel-footer-sub-menu">
				<div id="simpleweb-panel-footer-help-title"><?php _e( 'Need help?', 'simpleweb' ); ?></div>
				<div id="simpleweb-panel-footer-watch-tutorial" class="simpleweb-panel-footer-sub-menu-item">
					<i class="simpleweb-icon fa fa-video-camera"></i>
					<span class="simpleweb-title"><?php _e( 'Take a tour', 'simpleweb' ); ?></span>
				</div>
				<div class="simpleweb-panel-footer-sub-menu-item">
					<i class="simpleweb-icon fa fa-external-link"></i>
					<a class="simpleweb-title" href="https://go.simpleweb.com/docs" target="_blank"><?php _e( 'Go to the Documentation', 'simpleweb' ); ?></a>
				</div>
			</div>
		</div>
	</div>
	<div id="simpleweb-panel-footer-templates" class="simpleweb-panel-footer-tool" title="<?php esc_attr_e( 'Templates', 'simpleweb' ); ?>">
		<span class="simpleweb-screen-only"><?php _e( 'Templates', 'simpleweb' ); ?></span>
		<i class="fa fa-folder"></i>
		<div class="simpleweb-panel-footer-sub-menu-wrapper">
			<div class="simpleweb-panel-footer-sub-menu">
				<div id="simpleweb-panel-footer-templates-modal" class="simpleweb-panel-footer-sub-menu-item">
					<i class="simpleweb-icon fa fa-folder"></i>
					<span class="simpleweb-title"><?php _e( 'Templates Library', 'simpleweb' ); ?></span>
				</div>
				<div id="simpleweb-panel-footer-save-template" class="simpleweb-panel-footer-sub-menu-item">
					<i class="simpleweb-icon fa fa-save"></i>
					<span class="simpleweb-title"><?php _e( 'Save Template', 'simpleweb' ); ?></span>
				</div>
			</div>
		</div>
	</div>
	<div id="simpleweb-panel-footer-save" class="simpleweb-panel-footer-tool" title="<?php esc_attr_e( 'Save', 'simpleweb' ); ?>">
		<button class="simpleweb-button">
			<span class="simpleweb-state-icon">
				<i class="fa fa-spin fa-circle-o-notch "></i>
			</span>
			<?php _e( 'Save', 'simpleweb' ); ?>
		</button>
		<?php /*<div class="simpleweb-panel-footer-sub-menu-wrapper">
			<div class="simpleweb-panel-footer-sub-menu">
				<div id="simpleweb-panel-footer-publish" class="simpleweb-panel-footer-sub-menu-item">
					<i class="simpleweb-icon fa fa-check-circle"></i>
					<span class="simpleweb-title"><?php _e( 'Publish', 'simpleweb' ); ?></span>
				</div>
				<div id="simpleweb-panel-footer-discard" class="simpleweb-panel-footer-sub-menu-item">
					<i class="simpleweb-icon fa fa-times-circle"></i>
					<span class="simpleweb-title"><?php _e( 'Discard', 'simpleweb' ); ?></span>
				</div>
			</div>
		</div>*/ ?>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-mode-switcher-content">
	<input id="simpleweb-mode-switcher-preview-input" type="checkbox">
	<label for="simpleweb-mode-switcher-preview-input" id="simpleweb-mode-switcher-preview" title="<?php esc_attr_e( 'Preview', 'simpleweb' ); ?>">
		<span class="simpleweb-screen-only"><?php _e( 'Preview', 'simpleweb' ); ?></span>
		<i class="fa"></i>
	</label>
</script>

<script type="text/template" id="tmpl-editor-content">
	<div class="simpleweb-panel-navigation">
		<# _.each( elementData.tabs_controls, function( tabTitle, tabSlug ) { #>
		<div class="simpleweb-panel-navigation-tab simpleweb-tab-control-{{ tabSlug }}" data-tab="{{ tabSlug }}">
			<a href="#">{{{ tabTitle }}}</a>
		</div>
		<# } ); #>
	</div>
	<# if ( elementData.reload_preview ) { #>
		<div class="simpleweb-update-preview">
			<div class="simpleweb-update-preview-title"><?php echo __( 'Update changes to page', 'simpleweb' ); ?></div>
			<div class="simpleweb-update-preview-button-wrapper">
				<button class="simpleweb-update-preview-button simpleweb-button simpleweb-button-success"><?php echo __( 'Apply', 'simpleweb' ); ?></button>
			</div>
		</div>
	<# } #>
	<div id="simpleweb-controls"></div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-schemes-disabled">
	<i class="simpleweb-panel-nerd-box-icon eicon-nerd"></i>
	<div class="simpleweb-panel-nerd-box-title">{{{ '<?php echo __( '{0} are disabled', 'simpleweb' ); ?>'.replace( '{0}', disabledTitle ) }}}</div>
	<div class="simpleweb-panel-nerd-box-message"><?php printf( __( 'You can enable it from the <a href="%s" target="_blank">SimpleWeb settings page</a>.', 'simpleweb' ), Settings::get_url() ); ?></div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-scheme-color-item">
	<div class="simpleweb-panel-scheme-color-input-wrapper">
		<input type="text" class="simpleweb-panel-scheme-color-value" value="{{ value }}" data-alpha="true" />
	</div>
	<div class="simpleweb-panel-scheme-color-title">{{{ title }}}</div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-scheme-typography-item">
	<div class="simpleweb-panel-heading">
		<div class="simpleweb-panel-heading-toggle">
			<i class="fa"></i>
		</div>
		<div class="simpleweb-panel-heading-title">{{{ title }}}</div>
	</div>
	<div class="simpleweb-panel-scheme-typography-items simpleweb-panel-box-content">
		<?php
		$scheme_fields_keys = Group_Control_Typography::get_scheme_fields_keys();

		$typography_group = Plugin::$instance->controls_manager->get_control_groups( 'typography' );

		$typography_fields = $typography_group->get_fields();

		$scheme_fields = array_intersect_key( $typography_fields, array_flip( $scheme_fields_keys ) );

		$system_fonts = Fonts::get_fonts_by_groups( [ Fonts::SYSTEM ] );

		$google_fonts = Fonts::get_fonts_by_groups( [ Fonts::GOOGLE, Fonts::EARLYACCESS ] );

		foreach ( $scheme_fields as $option_name => $option ) : ?>
			<div class="simpleweb-panel-scheme-typography-item">
				<div class="simpleweb-panel-scheme-item-title simpleweb-control-title"><?php echo $option['label']; ?></div>
				<div class="simpleweb-panel-scheme-typography-item-value">
					<?php if ( 'select' === $option['type'] ) : ?>
						<select name="<?php echo $option_name; ?>" class="simpleweb-panel-scheme-typography-item-field">
							<?php foreach ( $option['options'] as $field_key => $field_value ) : ?>
								<option value="<?php echo $field_key; ?>"><?php echo $field_value; ?></option>
							<?php endforeach; ?>
						</select>
					<?php elseif ( 'font' === $option['type'] ) : ?>
						<select name="<?php echo $option_name; ?>" class="simpleweb-panel-scheme-typography-item-field">
							<option value=""><?php _e( 'Default', 'simpleweb' ); ?></option>

							<optgroup label="<?php _e( 'System', 'simpleweb' ); ?>">
								<?php foreach ( $system_fonts as $font_title => $font_type ) : ?>
									<option value="<?php echo esc_attr( $font_title ); ?>"><?php echo $font_title; ?></option>
								<?php endforeach; ?>
							</optgroup>

							<optgroup label="<?php _e( 'Google', 'simpleweb' ); ?>">
								<?php foreach ( $google_fonts as $font_title => $font_type ) : ?>
									<option value="<?php echo esc_attr( $font_title ); ?>"><?php echo $font_title; ?></option>
								<?php endforeach; ?>
							</optgroup>
						</select>
					<?php elseif ( 'text' === $option['type'] ) : ?>
						<input name="<?php echo $option_name; ?>" class="simpleweb-panel-scheme-typography-item-field" />
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-control-responsive-switchers">
	<div class="simpleweb-control-responsive-switchers">
		<a class="simpleweb-responsive-switcher simpleweb-responsive-switcher-desktop" data-device="desktop">
			<i class="eicon-device-desktop"></i>
		</a>
		<a class="simpleweb-responsive-switcher simpleweb-responsive-switcher-tablet" data-device="tablet">
			<i class="eicon-device-tablet"></i>
		</a>
		<a class="simpleweb-responsive-switcher simpleweb-responsive-switcher-mobile" data-device="mobile">
			<i class="eicon-device-mobile"></i>
		</a>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-revisions">
	<div class="simpleweb-panel-scheme-buttons">
		<div class="simpleweb-panel-scheme-button-wrapper simpleweb-panel-scheme-discard">
			<button class="simpleweb-button" disabled>
				<i class="fa fa-times"></i><?php _e( 'Discard', 'simpleweb' ); ?>
			</button>
		</div>
		<div class="simpleweb-panel-scheme-button-wrapper simpleweb-panel-scheme-save">
			<button class="simpleweb-button simpleweb-button-success" disabled>
				<?php _e( 'Apply', 'simpleweb' ); ?>
			</button>
		</div>
	</div>
	<div class="simpleweb-panel-box">
		<div class="simpleweb-panel-heading">
			<div class="simpleweb-panel-heading-title"><?php _e( 'Revision History', 'simpleweb' ); ?></div>
		</div>
		<div id="simpleweb-revisions-list" class="simpleweb-panel-box-content"></div>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-page-settings">
	<div class="simpleweb-panel-navigation">
		<# _.each( simpleweb.config.page_settings.tabs, function( tabTitle, tabSlug ) { #>
			<div class="simpleweb-panel-navigation-tab simpleweb-tab-control-{{ tabSlug }}" data-tab="{{ tabSlug }}">
				<a href="#">{{{ tabTitle }}}</a>
			</div>
			<# } ); #>
	</div>
	<div id="simpleweb-panel-page-settings-controls"></div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-revisions-no-revisions">
	<i class="simpleweb-panel-nerd-box-icon eicon-nerd"></i>
	<div class="simpleweb-panel-nerd-box-title"><?php _e( 'No Revisions Saved Yet', 'simpleweb' ); ?></div>
	<div class="simpleweb-panel-nerd-box-message">{{{ simpleweb.translate( simpleweb.config.revisions_enabled ? 'no_revisions_1' : 'revisions_disabled_1' ) }}}</div>
	<div class="simpleweb-panel-nerd-box-message">{{{ simpleweb.translate( simpleweb.config.revisions_enabled ? 'no_revisions_2' : 'revisions_disabled_2' ) }}}</div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-revisions-revision-item">
	<div class="simpleweb-revision-item__gravatar">{{{ gravatar }}}</div>
	<div class="simpleweb-revision-item__details">
		<div class="simpleweb-revision-date">{{{ date }}}</div>
		<div class="simpleweb-revision-meta">{{{ simpleweb.translate( type ) }}} <?php _e( 'By', 'simpleweb' ); ?> {{{ author }}}</div>
	</div>
	<div class="simpleweb-revision-item__tools">
		<i class="simpleweb-revision-item__tools-delete fa fa-times"></i>
		<i class="simpleweb-revision-item__tools-spinner fa fa-spin fa-circle-o-notch"></i>
	</div>
</script>
