<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<script type="text/template" id="tmpl-simpleweb-panel-elements">
	
    <div id="simpleweb-panel-elements-navigation" class="simpleweb-panel-navigation">
		<div id="simpleweb-panel-elements-navigation-all" class="simpleweb-panel-navigation-tab active" data-view="categories"><?php echo __( 'Elements', 'simpleweb' ); ?></div>
		<!-- <div id="simpleweb-panel-elements-navigation-global" class="simpleweb-panel-navigation-tab" data-view="global"><?php echo __( 'Global', 'simpleweb' ); ?></div> -->
	</div>
	<div id="simpleweb-panel-elements-search-area"></div>
	<div id="simpleweb-panel-elements-wrapper"></div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-categories">
	<div id="simpleweb-panel-categories"></div>
	<div id="simpleweb-panel-get-pro-elements" class="simpleweb-panel-nerd-box">
		<a class="simpleweb-button simpleweb-button-default simpleweb-panel-nerd-box-link" href="http://dev.simpleweb.com" target="_blank"><?php echo __( 'Upgrade', 'simpleweb' ); ?></a>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-elements-category">
	<div class="panel-elements-category-title panel-elements-category-title-{{ name }}">{{{ title }}}</div>
	<div class="panel-elements-category-items"></div>
</script>

<script type="text/template" id="tmpl-simpleweb-panel-element-search">
	<input id="simpleweb-panel-elements-search-input" placeholder="<?php _e( 'Search Widget...', 'simpleweb' ); ?>" />
	<i class="fa fa-search"></i>
</script>

<script type="text/template" id="tmpl-simpleweb-element-library-element">
	<div class="simpleweb-element">
		<div class="icon">
			<i class="{{ icon }}"></i>
		</div>
		<div class="simpleweb-element-title-wrapper">
			<div id="add-{{{ title }}}" class="title">{{{ title }}}</div>
		</div>
	</div>
</script>



<script type="text/template" id="tmpl-simpleweb-panel-global">
	<div class="simpleweb-panel-nerd-box">
		<i class="simpleweb-panel-nerd-box-icon eicon-hypster"></i>
		<a class="simpleweb-button simpleweb-button-default simpleweb-panel-nerd-box-link" href="http://dev.simpleweb.com" target="_blank"><?php echo __( 'Upgrade', 'simpleweb' ); ?></a>
	</div>
</script>



<script>
    $(document).on('click','#crew-add-page2',function(){
        $('#myModal').modal('show');
    });

    function tmce_getContent(editor_id, textarea_id) {
        debugger
        if ( typeof editor_id == 'undefined' ) editor_id = wpActiveEditor;
        if ( typeof textarea_id == 'undefined' ) textarea_id = editor_id;

        if ( jQuery('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id) ) {
            return tinyMCE.get(editor_id).getContent();
        }else{
            return jQuery('#'+textarea_id).val();
        }
    }

    $('#submit-form').click(function(e){

        debugger
        var target = $(this);
        var myData =  {
            action: 'my_ajax_action',
            post_title : $('input[name="title"]').val(), // PHP: $_POST['first_name']
            post_content  : tmce_getContent('mycustomeditor','mycustomeditor'), // PHP: $_POST['last_name']
            
        }
        var callBackUrl = "<?php echo admin_url('admin-ajax.php'); ?>";;
        e.preventDefault();
        $.ajax({
            type: "POST", // use $_POST method to submit data
            url: callBackUrl, // where to submit the data
            data : myData,
            success:function(data) {
//                debugger
                window.location.href = data;
                console.log(data); // the HTML result of that URL after submit the data
            },
            error: function(errorThrown){
                console.log(errorThrown); // error
            }
        });
    });

</script>
