<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>
<script type="text/template" id="tmpl-simpleweb-repeater-row">
	<div class="simpleweb-repeater-row-tools">
		<div class="simpleweb-repeater-row-handle-sortable">
			<i class="fa fa-ellipsis-v"></i>
		</div>
		<div class="simpleweb-repeater-row-item-title"></div>
		<div class="simpleweb-repeater-row-tool simpleweb-repeater-tool-duplicate">
			<i class="fa fa-copy"></i>
		</div>
		<div class="simpleweb-repeater-row-tool simpleweb-repeater-tool-remove">
			<i class="fa fa-remove"></i>
		</div>
	</div>
	<div class="simpleweb-repeater-row-controls"></div>
</script>
