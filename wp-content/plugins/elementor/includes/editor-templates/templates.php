<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<script type="text/template" id="tmpl-simpleweb-template-library-header">
	<div id="simpleweb-template-library-header-logo-area"></div>
	<div id="simpleweb-template-library-header-menu-area"></div>
	<div id="simpleweb-template-library-header-items-area">
		<div id="simpleweb-template-library-header-close-modal" class="simpleweb-template-library-header-item" title="<?php _e( 'Close', 'simpleweb' ); ?>">
			<i class="eicon-close" title="<?php _e( 'Close', 'simpleweb' ); ?>"></i>
		</div>
		<div id="simpleweb-template-library-header-tools"></div>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-header-logo">
	<i class="eicon-simpleweb-square"></i><span><?php _e( 'Library', 'simpleweb' ); ?></span>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-header-save">
	<i class="eicon-save" title="<?php _e( 'Save Template', 'simpleweb' ); ?>"></i>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-header-menu">
	<div id="simpleweb-template-library-menu-pre-made-templates" class="simpleweb-template-library-menu-item" data-template-source="remote"><?php _e( 'Predesigned Templates', 'simpleweb' ); ?></div>
	<div id="simpleweb-template-library-menu-my-templates" class="simpleweb-template-library-menu-item" data-template-source="local"><?php _e( 'My Templates', 'simpleweb' ); ?></div>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-header-preview">
	<div id="simpleweb-template-library-header-preview-insert-wrapper" class="simpleweb-template-library-header-item">
		{{{ simpleweb.templates.getLayout().getTemplateActionButton( obj ) }}}
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-header-back">
	<i class="eicon-"></i><span><?php _e( 'Back To library', 'simpleweb' ); ?></span>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-loading">
	<div class="simpleweb-loader-wrapper">
		<div class="simpleweb-loader">
			<div class="simpleweb-loader-box"></div>
			<div class="simpleweb-loader-box"></div>
			<div class="simpleweb-loader-box"></div>
			<div class="simpleweb-loader-box"></div>
		</div>
		<div class="simpleweb-loading-title"><?php _e( 'Loading', 'simpleweb' ) ?></div>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-templates">
	<div id="simpleweb-template-library-templates-container"></div>
	<div id="simpleweb-template-library-footer-banner">
		<i class="eicon-nerd"></i>
		<div class="simpleweb-excerpt"><?php echo __( 'Stay tuned! More awesome templates coming real soon.', 'simpleweb' ); ?></div>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-template-remote">
	<div class="simpleweb-template-library-template-body">
		<div class="simpleweb-template-library-template-screenshot" style="background-image: url({{ thumbnail }});"></div>
		<div class="simpleweb-template-library-template-controls">
			<div class="simpleweb-template-library-template-preview">
				<i class="fa fa-search-plus"></i>
			</div>
			{{{ simpleweb.templates.getLayout().getTemplateActionButton( obj ) }}}
		</div>
	</div>
	<div class="simpleweb-template-library-template-name">{{{ title }}}</div>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-insert-button">
	<button class="simpleweb-template-library-template-action simpleweb-template-library-template-insert simpleweb-button simpleweb-button-success">
		<i class="eicon-file-download"></i><span class="simpleweb-button-title"><?php _e( 'Insert', 'simpleweb' ); ?></span>
	</button>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-get-pro-button">
	<a href="https://go.simpleweb.com/pro-library/" target="_blank">
		<button class="simpleweb-template-library-template-action simpleweb-button simpleweb-button-go-pro">
			<i class="fa fa-external-link-square"></i><span class="simpleweb-button-title"><?php _e( 'Go Pro', 'simpleweb' ); ?></span>
		</button>
	</a>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-template-local">
	<div class="simpleweb-template-library-template-icon">
		<i class="fa fa-{{ 'section' === type ? 'columns' : 'file-text-o' }}"></i>
	</div>
	<div class="simpleweb-template-library-template-name">{{{ title }}}</div>
	<div class="simpleweb-template-library-template-type">{{{ simpleweb.translate( type ) }}}</div>
	<div class="simpleweb-template-library-template-controls">
		<button class="simpleweb-template-library-template-action simpleweb-template-library-template-insert simpleweb-button simpleweb-button-success">
			<i class="eicon-file-download"></i><span class="simpleweb-button-title"><?php _e( 'Insert', 'simpleweb' ); ?></span>
		</button>
		<div class="simpleweb-template-library-template-export">
			<a href="{{ export_link }}">
				<i class="fa fa-sign-out"></i><span class="simpleweb-template-library-template-control-title"><?php echo __( 'Export', 'simpleweb' ); ?></span>
			</a>
		</div>
		<div class="simpleweb-template-library-template-delete">
			<i class="fa fa-trash-o"></i><span class="simpleweb-template-library-template-control-title"><?php echo __( 'Delete', 'simpleweb' ); ?></span>
		</div>
		<div class="simpleweb-template-library-template-preview">
			<i class="eicon-zoom-in"></i><span class="simpleweb-template-library-template-control-title"><?php echo __( 'Preview', 'simpleweb' ); ?></span>
		</div>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-save-template">
	<div class="simpleweb-template-library-blank-title">{{{ title }}}</div>
	<div class="simpleweb-template-library-blank-excerpt">{{{ description }}}</div>
	<form id="simpleweb-template-library-save-template-form">
		<input type="hidden" name="post_id" value="<?php echo get_the_ID(); ?>">
		<input id="simpleweb-template-library-save-template-name" name="title" placeholder="<?php _e( 'Enter Template Name', 'simpleweb' ); ?>" required>
		<button id="simpleweb-template-library-save-template-submit" class="simpleweb-button simpleweb-button-success">
			<span class="simpleweb-state-icon">
				<i class="fa fa-spin fa-circle-o-notch "></i>
			</span>
			<?php _e( 'Save', 'simpleweb' ); ?>
		</button>
	</form>
	<div class="simpleweb-template-library-blank-footer">
		<?php _e( 'What is Library?', 'simpleweb' ); ?>
		<a class="simpleweb-template-library-blank-footer-link" href="https://go.simpleweb.com/docs-library/" target="_blank"><?php _e( 'Read our tutorial on using Library templates.', 'simpleweb' ); ?></a>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-import">
	<form id="simpleweb-template-library-import-form">
		<input type="file" name="file" />
		<input type="submit">
	</form>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-templates-empty">
	<div id="simpleweb-template-library-templates-empty-icon">
		<i class="eicon-nerd"></i>
	</div>
	<div class="simpleweb-template-library-blank-title"><?php _e( 'Haven’t Saved Templates Yet?', 'simpleweb' ); ?></div>
	<div class="simpleweb-template-library-blank-excerpt"><?php _e( 'This is where your templates should be. Design it. Save it. Reuse it.', 'simpleweb' ); ?></div>
	<div class="simpleweb-template-library-blank-footer">
		<?php _e( 'What is Library?', 'simpleweb' ); ?>
		<a class="simpleweb-template-library-blank-footer-link" href="https://go.simpleweb.com/docs-library/" target="_blank"><?php _e( 'Read our tutorial on using Library templates.', 'simpleweb' ); ?></a>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-template-library-preview">
	<iframe></iframe>
</script>
