<style>

    #simpleweb-custom-panel{
        position: fixed;
        height: 100%;
        top: 0;
        right: 0;
        bottom: 0;
        overflow: visible;
        background-color: #e6e9ec;
    }
</style>

<div id="simpleweb-custom-panel" class="simpleweb-panel ui-resizable"><div id="simpleweb-panel-inner">
        <div id="simpleweb-mode-switcher"><div id="simpleweb-mode-switcher-inner">
                <input id="simpleweb-mode-switcher-preview-input" type="checkbox">
                <label for="simpleweb-mode-switcher-preview-input" id="simpleweb-mode-switcher-preview" title="Preview">
                    <span class="simpleweb-screen-only">Preview</span>
                    <i class="fa"></i>
                </label>
            </div></div>
        <header id="simpleweb-panel-header-wrapper"><div id="simpleweb-panel-header">

                <!-- 	<div id="simpleweb-panel-header-title"></div> -->
                <div class="text-center panel-simpleheading"><h4 style="font-size: 18px;
	font-style: italic;">SimpleWeb </h4></div>
                <div id="simpleweb-panel-header-add-button" class="simpleweb-header-button">
                    <i class="simpleweb-icon eicon-apps tooltip-target" data-tooltip="Widgets Panel"></i>
                </div>
            </div></header>
        <main id="simpleweb-panel-content-wrapper" class="ps-container ps-theme-default ps-active-y" data-ps-id="3aae2082-f075-2679-8817-95324eed6fc9"><div>

                <div id="simpleweb-panel-elements-navigation" class="simpleweb-panel-navigation">
                    <div id="simpleweb-panel-elements-navigation-all" class="simpleweb-panel-navigation-tab active" data-view="categories">Custom Elements</div>
                    <!-- <div id="simpleweb-panel-elements-navigation-global" class="simpleweb-panel-navigation-tab" data-view="global">Global</div> -->
                </div>

                <div id="simpleweb-panel-elements-wrapper">
                    <div id="simpleweb-panel-elements-categories">
                        <div id="simpleweb-panel-categories"><div class="simpleweb-panel-category">
                            <div class="panel-elements-category-title panel-elements-category-title-basic">Basic</div>
                                <div class="panel-elements-category-items"><div class="simpleweb-element-wrapper" draggable="true">
                                        <div id="crew-add-page2" class="simpleweb-element">
                                            <div class="icon">
                                                <i class="eicon-wordpress"></i>
                                            </div>
                                            <div class="simpleweb-element-title-wrapper">
                                                <div id="add-Columns" class="title">Add Page</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simpleweb-element-wrapper" draggable="true">
                                        <div class="simpleweb-element">
                                            <div class="icon">
                                                <i class="eicon-wordpress"></i>
                                            </div>
                                            <div class="simpleweb-element-title-wrapper">
                                                <div id="add-Heading" class="title">Add Product</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px; height: 730px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 220px;"></div></div></main>


    </div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div></div>