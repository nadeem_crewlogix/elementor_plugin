<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo __( 'SimpleWeb', 'simpleweb' ) . ' | ' . get_the_title(); ?></title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"  >

	<?php wp_head(); ?>
</head>
<body class="simpleweb-editor-active">

<div id="simpleweb-editor-wrapper ">
    
    <div id="simpleweb-preview">
        <div id="simpleweb-loading">
            <div class="simpleweb-loader-wrapper">
                <div class="simpleweb-loader">
                    <div class="simpleweb-loader-box"></div>
                    <div class="simpleweb-loader-box"></div>
                    <div class="simpleweb-loader-box"></div>
                    <div class="simpleweb-loader-box"></div>
                </div>
                <div class="simpleweb-loading-title"><?php _e( 'Loading', 'simpleweb' ) ?></div>
            </div>
        </div>
        <div id="simpleweb-preview-responsive-wrapper" class="simpleweb-device-desktop simpleweb-device-rotate-portrait">
            <div id="simpleweb-preview-loading">
                <i class="fa fa-spin fa-circle-o-notch"></i>
            </div>
            <?php
			// IFrame will be create here by the Javascript later.
			?>
        </div>
    </div>
	<div id="simpleweb-panel" class="simpleweb-panel"></div>
    <?php include ('custom-element.php');?>
</div>





<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form method="POST" >
                <input type="hidden" name="post_type" value="published">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Title:</h4>
                    <p><input type="text" value="" name="title"></p>
                </div>
                <div class="modal-body">
                    <p><?php
                        $content   = '';
                        $editor_id = 'mycustomeditor';
                        wp_editor( $content, $editor_id );
                        ?></p>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submit-form" class="btn btn-primary" name="submit" data-dismiss="modal">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>




<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/bootstrap.min.js'; ?>"> </script>
</body>
</html>
