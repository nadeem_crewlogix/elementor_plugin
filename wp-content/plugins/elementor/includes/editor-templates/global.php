<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<script type="text/template" id="tmpl-simpleweb-empty-preview">
	<div class="simpleweb-first-add">
		<div class="simpleweb-icon eicon-plus"></div>
	</div>
</script>

<script type="text/template" id="tmpl-simpleweb-preview">
	<div class="simpleweb-section-wrap"></div>
</script>

<script type="text/template" id="tmpl-simpleweb-add-section">
	<div class="simpleweb-add-section-inner">
		<div class="simpleweb-add-section-close">
			<i class="fa fa-times"></i>
		</div>
		<div class="simpleweb-add-new-section">
			<button class="simpleweb-add-section-button simpleweb-button"><?php _e( 'Add New Section', 'simpleweb' ); ?></button>
			<button class="simpleweb-add-template-button simpleweb-button"><?php _e( 'Add Template', 'simpleweb' ); ?></button>
			<div class="simpleweb-add-section-drag-title"><?php _e( 'Or drag widget here', 'simpleweb' ); ?></div>
		</div>
		<div class="simpleweb-select-preset">
			<div class="simpleweb-select-preset-title"><?php _e( 'Select your Structure', 'simpleweb' ); ?></div>
			<ul class="simpleweb-select-preset-list">
				<#
					var structures = [ 10, 20, 30, 40, 21, 22, 31, 32, 33, 50, 60, 34 ];

					_.each( structures, function( structure ) {
					var preset = simpleweb.presetsFactory.getPresetByStructure( structure ); #>

					<li class="simpleweb-preset simpleweb-column simpleweb-col-16" data-structure="{{ structure }}">
						{{{ simpleweb.presetsFactory.getPresetSVG( preset.preset ).outerHTML }}}
					</li>
					<# } ); #>
			</ul>
		</div>
	</div>
</script>
