<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A draggable Range Slider control.
 *
 * @param array  $default    {
 *
 * 		@type integer $size       The initial value of slider
 *                           	  Default empty
 * }
 *
 * @since              1.0.0
 */
class Control_Slider extends Control_Base_Units {

	public function get_type() {
		return 'slider';
	}

	public function get_default_value() {
		return array_merge( parent::get_default_value(), [
			'size' => '',
		] );
	}

	protected function get_default_settings() {
		return array_merge( parent::get_default_settings(), [
			'label_block' => true,
		] );
	}

	public function content_template() {
		$control_uid = $this->get_control_uid();
		?>
		<div class="simpleweb-control-field">
			<label for="<?php echo $control_uid; ?>" class="simpleweb-control-title">{{{ data.label }}}</label>
			<?php $this->print_units_template(); ?>
			<div class="simpleweb-control-input-wrapper simpleweb-clearfix">
				<div class="simpleweb-slider"></div>
				<div class="simpleweb-slider-input">
					<input id="<?php echo $control_uid; ?>" type="number" min="{{ data.min }}" max="{{ data.max }}" step="{{ data.step }}" data-setting="size" />
				</div>
			</div>
		</div>
		<# if ( data.description ) { #>
		<div class="simpleweb-control-field-description">{{{ data.description }}}</div>
		<# } #>
		<?php
	}
}
