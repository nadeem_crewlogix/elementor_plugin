<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A private control for section columns structure.
 *
 * @since 1.0.0
 */
class Control_Structure extends Base_Data_Control {

	public function get_type() {
		return 'structure';
	}

	public function content_template() {
		$preset_control_uid = $this->get_control_uid( '{{ preset.key }}' );
		?>
		<div class="simpleweb-control-field">
			<div class="simpleweb-control-input-wrapper">
				<div class="simpleweb-control-structure-title"><?php _e( 'Structure', 'simpleweb' ); ?></div>
				<# var currentPreset = simpleweb.presetsFactory.getPresetByStructure( data.controlValue ); #>
				<div class="simpleweb-control-structure-preset simpleweb-control-structure-current-preset">
					{{{ simpleweb.presetsFactory.getPresetSVG( currentPreset.preset, 233, 72, 5 ).outerHTML }}}
				</div>
				<div class="simpleweb-control-structure-reset"><i class="fa fa-undo"></i><?php _e( 'Reset Structure', 'simpleweb' ); ?></div>
				<#
				var morePresets = getMorePresets();

				if ( morePresets.length > 1 ) { #>
					<div class="simpleweb-control-structure-more-presets-title"><?php _e( 'More Structures', 'simpleweb' ); ?></div>
					<div class="simpleweb-control-structure-more-presets">
						<# _.each( morePresets, function( preset ) { #>
							<div class="simpleweb-control-structure-preset-wrapper">
								<input id="<?php echo $preset_control_uid; ?>" type="radio" name="simpleweb-control-structure-preset-{{ data._cid }}" data-setting="structure" value="{{ preset.key }}">
								<label for="<?php echo $preset_control_uid; ?>" class="simpleweb-control-structure-preset">
									{{{ simpleweb.presetsFactory.getPresetSVG( preset.preset, 102, 42 ).outerHTML }}}
								</label>
								<div class="simpleweb-control-structure-preset-title">{{{ preset.preset.join( ', ' ) }}}</div>
							</div>
						<# } ); #>
					</div>
				<# } #>
			</div>
		</div>
		
		<# if ( data.description ) { #>
			<div class="simpleweb-control-field-description">{{{ data.description }}}</div>
		<# } #>
		<?php
	}

	protected function get_default_settings() {
		return [
			'separator' => 'none',
			'label_block' => true,
		];
	}
}
