<?php
namespace SimpleWeb;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A classic Textarea control.
 *
 * @param string  $default    A default value
 *                            Default empty
 * @param integer $rows       Number of rows
 *                            Default 5
 *
 * @since 1.0.0
 */
class Control_Textarea extends Base_Data_Control {

	public function get_type() {
		return 'textarea';
	}

	protected function get_default_settings() {
		return [
			'label_block' => true,
		];
	}

	public function content_template() {
		$control_uid = $this->get_control_uid();
		?>
		<div class="simpleweb-control-field">
			<label for="<?php echo $control_uid; ?>" class="simpleweb-control-title">{{{ data.label }}}</label>
			<div class="simpleweb-control-input-wrapper">
				<textarea id="<?php echo $control_uid; ?>" rows="{{ data.rows || 5 }}" data-setting="{{ data.name }}" placeholder="{{ data.placeholder }}"></textarea>
			</div>
		</div>
		<# if ( data.description ) { #>
		<div class="simpleweb-control-field-description">{{{ data.description }}}</div>
		<# } #>
		<?php
	}
}
