<?php
/**
 * Plugin Name: SimpleWeb
 * Description: The most advanced frontend drag & drop page builder. Create high-end, pixel perfect websites at record speeds. Any theme, any page, any design.
 * Plugin URI: https://simpleweb.com/
 * Author: SimpleWeb.com
 * Version: 1.5.5
 * Author URI: https://simpleweb.com/
 *
 * Text Domain: simpleweb
 *
 * SimpleWeb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * SimpleWeb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define( 'ELEMENTOR_VERSION', '1.5.5' );
define( 'ELEMENTOR_PREVIOUS_STABLE_VERSION', '1.4.10' );

define( 'ELEMENTOR__FILE__', __FILE__ );
define( 'ELEMENTOR_PLUGIN_BASE', plugin_basename( ELEMENTOR__FILE__ ) );
define( 'ELEMENTOR_URL', plugins_url( '/', ELEMENTOR__FILE__ ) );
define( 'ELEMENTOR_PATH', plugin_dir_path( ELEMENTOR__FILE__ ) );
define( 'ELEMENTOR_ASSETS_URL', ELEMENTOR_URL . 'assets/' );

add_action( 'plugins_loaded', 'simpleweb_load_plugin_textdomain' );

if ( ! version_compare( PHP_VERSION, '5.4', '>=' ) ) {
	add_action( 'admin_notices', 'simpleweb_fail_php_version' );
} elseif ( ! version_compare( get_bloginfo( 'version' ), '4.5', '>=' ) ) {
	add_action( 'admin_notices', 'simpleweb_fail_wp_version' );
} else {
	require( ELEMENTOR_PATH . 'includes/plugin.php' );
}

/**
 * Load gettext translate for our text domain.
 *
 * @since 1.0.0
 *
 * @return void
 */
function simpleweb_load_plugin_textdomain() {
	load_plugin_textdomain( 'simpleweb' );
}

/**
 * Show in WP Dashboard notice about the plugin is not activated (PHP version).
 *
 * @since 1.0.0
 *
 * @return void
 */
function simpleweb_fail_php_version() {
	$message = esc_html__( 'SimpleWeb requires PHP version 5.4+, plugin is currently NOT ACTIVE.', 'simpleweb' );
	$html_message = sprintf( '<div class="error">%s</div>', wpautop( $message ) );
	echo wp_kses_post( $html_message );
}

/**
 * Show in WP Dashboard notice about the plugin is not activated (WP version).
 *
 * @since 1.5.0
 *
 * @return void
 */
function simpleweb_fail_wp_version() {
	$message = sprintf( esc_html__( 'SimpleWeb requires WordPress version %s+. Because you are using an earlier version, the plugin is currently NOT ACTIVE.', 'simpleweb' ), '4.5' );
	$html_message = sprintf( '<div class="error">%s</div>', wpautop( $message ) );
	echo wp_kses_post( $html_message );
}
